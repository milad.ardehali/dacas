package com.digilion.dataAccessClass.exceptions;

/**
 * DacasException
 * @version 1
 * @since 17/01/2020
 * @author Milad ARDEHALI
 */
public class DacasException extends RuntimeException {

    /**
     * serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * DacasException
     * @param message The message explaining the reason for the exception.
     * @version 1
     */
    public DacasException(String message) {
        super(message);
    }

    /**
     * DacasException
     * @param message The message explaining the reason for the exception.
     * @param cause The underlying cause.
     * @version 1
     */
    public DacasException(String message, Throwable cause) {
        super(message, cause);
    }

}