package com.digilion.dataAccessClass.enumerations;

/**
 * Support that can be used
 * <ul>
 *      <li>File {@link #FILE}</li>
 *      <li>Memory {@link #MEMORY}</li>
 * </ul>
 * @version 1
 * @since 16/02/2020
 * @author Milad ARDEHALI
 */
public enum DacaStorageSupportEnum {
    /**
     * File and memory support
     */
    FILE,
    /**
     * Only memory support
     */
    MEMORY;

}
