package com.digilion.dataAccessClass.transaction;

import com.digilion.dataAccessClass.element.DacasElement;
import com.digilion.dataAccessClass.enumerations.DacaStorageSupportEnum;
import com.digilion.dataAccessClass.manager.DacasManager;
import com.digilion.dataAccessClass.exceptions.DacasException;

import java.time.LocalDateTime;
import java.util.*;

/**
 * DacasTransaction
 * <p>
 * Manager of transactions
 * <ul>
 *     <li>fileTransaction</li>
 *     <li>memoryTransaction</li>
 *     <li>storageSupport</li>
 *     <li>maxObject</li>
 * </ul>
 *
 * @author milad-ardehali
 * @version 1
 * @since 17/01/2020
 */
public class DacasTransaction {

    private static FileTransaction fileTransaction;
    private static MemoryTransaction memoryTransaction;
    private static DacaStorageSupportEnum storageSupport;
    private static int maxObject;
    private static Long ttl;


    private DacasTransaction() {
        super();
    }

    /**
     * constructor of DacasTransaction
     *
     * @param storageSupport storage support {@link DacaStorageSupportEnum}
     * @param maxObject max object in memory
     * @param propertiesFileName properties file name
     * @param defaultTTL default time to live
     */
    public DacasTransaction(DacaStorageSupportEnum storageSupport, int maxObject, String propertiesFileName, Long defaultTTL) {
        this.storageSupport = storageSupport;
        this.maxObject = maxObject;
        this.ttl = defaultTTL;
        this.memoryTransaction = new MemoryTransaction(defaultTTL);
        if (storageSupport.equals(DacaStorageSupportEnum.FILE)) {
            Objects.requireNonNull(propertiesFileName);
            this.fileTransaction = new FileTransaction(propertiesFileName, defaultTTL);
        }
    }

    /**
     * Read dacas file and add values into memory
     *
     * @throws DacasException Dacas Exception
     */
    public void read() throws DacasException {
        HashMap<String, DacasElement> listDataLoaded = fileTransaction.read();
        memoryTransaction.commitMulti(listDataLoaded);
        purge();
        if(listDataLoaded.size() > this.maxObject) {
            memoryTransaction.cleanup();
            throw new DacasException("The number of loaded elements in dacas file exceeds the maximum limit set in memory.");
        }
    }

    /**
     * Get value from dacas memory storage by key
     *
     * @param key the key of value in memory
     * @return value
     * @throws DacasException Dacas Exception
     */
    public static Object get(String key) throws DacasException {
        return memoryTransaction.get(key);
    }

    /**
     * Get all key/values from dacas memory storage
     *
     * @return map of key/value
     * @throws DacasException Dacas Exception
     */
    public static HashMap<String, Object> getAll() throws DacasException {
        return memoryTransaction.getAll();
    }

    /**
     * Record single DacasElement element into dacas memory storage
     *
     * @param key the key of the value
     * @param dacasElement dacas element
     * @throws DacasException Dacas Exception
     * @version 1
     * @since 16/02/2020
     */
    public static void commit(String key, DacasElement dacasElement) throws DacasException {
        checkLimitSize(1);
        memoryTransaction.commit(key, dacasElement);
    }

    /**
     * Record single Object element into dacas memory storage
     *
     * @param key the key of the value
     * @param object generic object
     * @throws DacasException Dacas Exception
     * @version 1
     * @since 16/02/2020
     */
    public static void commit(String key, Object object) throws DacasException {
        checkLimitSize(1);
        commit(key, DacasManager.buildDacasElement(object, LocalDateTime.now().plusSeconds(ttl)));
    }

    /**
     * Record element in dacas memory storage and push into dacas file after purge expired values
     *
     * @param key the key of the value
     * @param dacasElement dacas element
     * @throws DacasException Dacas Exception
     */
    public static void commitAndPush(String key, DacasElement dacasElement) throws DacasException {
        checkLimitSize(1);
        memoryTransaction.commit(key, dacasElement);
        if (storageSupport.equals(DacaStorageSupportEnum.FILE)) {
            purge();
            fileTransaction.push(memoryTransaction.getAllDacasValues());
        }

    }

    /**
     * Record multiple elements in dacas memory storage
     *
     * @param mapDacasElements map of dacas element
     * @throws DacasException Dacas Exception
     * @version 1
     * @since 16/02/2020
     */
    public static void commitMulti(HashMap<String, DacasElement> mapDacasElements) throws DacasException {
        checkLimitSize(mapDacasElements.size());
        memoryTransaction.commitMulti(mapDacasElements);
    }

    /**
     * Record multiple elements in dacas memory storage and push into dacas file after purge expired values
     *
     * @param mapDacasElements map of dacas element
     * @throws DacasException Dacas Exception
     * @version 1
     * @since 16/02/2020
     */
    public static void commitMultiAndPush(HashMap<String, DacasElement> mapDacasElements) throws DacasException {
        checkLimitSize(mapDacasElements.size());
        memoryTransaction.commitMulti(mapDacasElements);
        if (storageSupport.equals(DacaStorageSupportEnum.FILE))
            purge();
        fileTransaction.push(memoryTransaction.getAllDacasValues());
    }

    /**
     * Update DacasElement in dacas memory storage
     *
     * @param key the key of the value
     * @param value the value linked to key
     */
    public static void update(String key, DacasElement value) {
        memoryTransaction.update(key, value);
    }

    /**
     * Update multiple DacasElement in dacas memory storage
     *
     * @param mapDacasElements map of dacas element
     */
    public static void updateMulti(HashMap<String, DacasElement> mapDacasElements) {
        memoryTransaction.updateMulti(mapDacasElements);
    }

    /**
     * Upadte elements in dacas memory storage and push into dacas file after purge expired values
     *
     * @param key the key of the value
     * @param value the value linked to key
     * @throws DacasException Dacas Exception
     */
    public static void updateAndPush(String key, DacasElement value) throws DacasException {
        memoryTransaction.update(key, value);
        if (storageSupport.equals(DacaStorageSupportEnum.FILE)) {
            purge();
            fileTransaction.push(memoryTransaction.getAllDacasValues());
        }
    }

    /**
     * Upadte multiple elements in dacas memory storage and push into dacas file after purge expired values
     *
     * @param mapDacasElements map contains multiple DacasElement
     * @throws DacasException Dacas Exception
     */
    public static void updateMultiAndPush(HashMap<String, DacasElement> mapDacasElements) throws DacasException {
        memoryTransaction.updateMulti(mapDacasElements);
        if (storageSupport.equals(DacaStorageSupportEnum.FILE)) {
            purge();
            fileTransaction.push(memoryTransaction.getAllDacasValues());
        }
    }

    /**
     * Remove value in dacas memory storage by key
     *
     * @param keyToRemove string key to remove
     */
    public static void remove(String keyToRemove) {
        memoryTransaction.remove(keyToRemove);

    }

    /**
     * Remove value in dacas memory storage by key and push into dacas file after purge expired values
     *
     * @param keyToRemove string key to remove
     * @throws DacasException Dacas Exception
     */
    public static void removeAndPush(String keyToRemove) throws DacasException {
        memoryTransaction.remove(keyToRemove);
        if (storageSupport.equals(DacaStorageSupportEnum.FILE)) {
            purge();
            fileTransaction.push(memoryTransaction.getAllDacasValues());
        }

    }

    /**
     * Remove multiple values in dacas memory storage by key
     *
     * @param listKeyToRemove string key to remove
     */
    public static void removeMulti(List<String> listKeyToRemove) {
        listKeyToRemove.forEach(key -> remove(key));

    }

    /**
     * Remove multiple values in dacas memory storage by key and push into dacas file after purge expired values
     *
     * @param listKeyToRemove string key to remove
     * @throws DacasException Dacas Exception
     */
    public static void removeMultiAndPush(List<String> listKeyToRemove) throws DacasException {
        listKeyToRemove.forEach(key -> remove(key));
        if (storageSupport.equals(DacaStorageSupportEnum.FILE)) {
            purge();
            fileTransaction.push(memoryTransaction.getAllDacasValues());
        }
    }

    /**
     * Push values from dacas memory storage to dacas file storage
     * <p>
     * Nothing doing if dacas memory storage is configured in memory
     *
     * @throws DacasException Dacas Exception
     */
    public static void push() throws DacasException {
        if (storageSupport.equals(DacaStorageSupportEnum.FILE)) {
            purge();
            fileTransaction.push(memoryTransaction.getAllDacasValues());
        }
    }

    /**
     * Check if dacas memory storage contains this key
     *
     * @param key key whose existence we want to verify
     * @return boolean
     */
    public static boolean isExist(String key) throws DacasException {
        return memoryTransaction.isExist(key);
    }

    /**
     * Get nombre of element into dacas memory storage
     *
     * @return size
     */
    public static int size() throws DacasException {
        return memoryTransaction.size();
    }

    /**
     * Remove all element contains into dacas memory storage
     * <div>
     *     <ul>
     *         <li>If storageSupport is MEMORY: Remove all values from dacas memory storage</li>
     *         <li>If storageSupport is FILE: Delete dacasStorage file by new one</li>
     *     </ul>
     * </div>
     * @return boolean
     */
    public static boolean cleanup() {
        if (DacasManager.storageSupport.equals(DacaStorageSupportEnum.MEMORY)) {
            memoryTransaction.cleanup();
            return true;
        } else if (DacasManager.storageSupport.equals(DacaStorageSupportEnum.FILE)) {
            try {
                fileTransaction.removeDacasFile();
                fileTransaction.createNewDacasFile();
            } catch (DacasException e) {
                e.printStackTrace();
                return false;
            } finally {
                memoryTransaction.cleanup();
                return true;
            }
        }
        return false;
    }

    /**
     * Purge all expired elements from dacas memory storage
     *
     * @return numbre of purged element
     */
    public static List<String> purge() {
        return memoryTransaction.purge();
    }

    /**
     * Create backup of dacas file storage
     *
     * @return boolean (true is backup is created, else false)
     */
    public static boolean backup() {
        if (DacasManager.storageSupport.equals(DacaStorageSupportEnum.FILE))
            return fileTransaction.backup();
        else
            return false;
    }

    /**
     * Check if limit of memory size is not (atteindre) after added elements
     * @param nbreElementToAdd numbre of element to add in memory
     * @throws DacasException Dacas Exception
     */
    public static void checkLimitSize(int nbreElementToAdd) throws DacasException{
        if((size()+nbreElementToAdd)>maxObject)
            throw new DacasException("The number of loaded elements exceeds the maximum limit set.");
    }


}


