package com.digilion.dataAccessClass.transaction;

import com.digilion.dataAccessClass.element.DacasElement;
import com.digilion.dataAccessClass.element.ManagerInMemoryCache;
import com.digilion.dataAccessClass.exceptions.DacasException;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * A thread-safe class that will append JSON transaction data to a file.
 *
 * @author milad-ardehali
 * @version 1
 * @since 17/01/2020
 */
public class MemoryTransaction {

    private ManagerInMemoryCache<String> managerInMemoryCache;
    private Long defaultTTL;

    /**
     * Default no argument constructor
     */
    private MemoryTransaction() {
        super();
    }

    /**
     * Constructor
     * @param defaultTTL default time to live
     */
    public MemoryTransaction(Long defaultTTL) {
        this.defaultTTL = defaultTTL;
        this.managerInMemoryCache = new ManagerInMemoryCache<>();
    }

    /**
     * Commmit(String key, DaCaSElement daCaSElement)
     * <p>
     * <span>Value of type DaCaSElement is recommanded</span>
     * <p>
     * <span>Add daCaSElement to memory with string key</span>
     *
     * @param key key of element
     * @param daCaSElement value
     */
    public void commit(String key, DacasElement daCaSElement) {
        if(daCaSElement.getExpiredDateTime() == null) {
            daCaSElement.setExpiredDateTime(LocalDateTime.now().plusSeconds(this.defaultTTL));
        }
        managerInMemoryCache.put(key, daCaSElement);
    }

    /**
     * Add multiple values into dacas memory storage
     *
     * @param mapElements map containing values
     */
    public void commitMulti(HashMap<String, DacasElement> mapElements) {
        mapElements.forEach((k, v) -> {
                if (v.getExpiredDateTime() == null) {
                    v.setExpiredDateTime(LocalDateTime.now().plusSeconds(this.defaultTTL));
                }
        });
        managerInMemoryCache.putAll(mapElements);
    }

    /**
     * Returns the value linked to the key
     *
     * @param key key to get value
     * @return value
     */
    public Object get(String key) {
        return (managerInMemoryCache.get(key) != null ? managerInMemoryCache.get(key).getValue() : null);

    }

    /**
     * Returns values linked to the key
     *
     * @return map containing values
     */
    public HashMap<String, Object> getAll() {
        return managerInMemoryCache.getAll();

    }

    /**
     * Returns all dacasValues
     *
     * @return map containing values
     */
    public HashMap<String, DacasElement> getAllDacasValues() {
        return managerInMemoryCache.getAllDaCaSValues();

    }


    /**
     * If key exist, remove the value linked to the key
     *
     * @param key key to get value
     */
    public void remove(String key) {
        managerInMemoryCache.remove(key);
    }

    /**
     * Methode get number of value into dacas memory storage
     *
     * @return numbre of element in memory
     */
    public int size() {
        return managerInMemoryCache.size();
    }

    /**
     * Remove all elements saved  into dacas memory storage
     */
    public void cleanup() {
        managerInMemoryCache.cleanup();
    }

    /**
     * Purge all expired elements of dacas memory storage
     *
     * @return list of removed key
     */
    public List<String> purge() {
        return managerInMemoryCache.purge();
    }

    /**
     * Check if dacas memory storage contains this key
     * @param key map containing values
     * @return boolean if element exist with this key
     */
    public boolean isExist(String key) {
        return managerInMemoryCache.isExist(key);
    }

    /**
     * Update value on dacas memory storage by key
     * @param key map containing values
     * @param value value of dacas element linking to the key
     * @return boolean return true if element is updated
     * @throws DacasException dacasException if element not already exist
     */
    public boolean update(String key, DacasElement value) throws DacasException{
        if(!isExist(key)) {
            throw new DacasException(String.format("Can not update elemment %s because it not already exist in memory. First commit it", key));
        }
        return managerInMemoryCache.update(key, value);
    }

    /**
     * Update multiple values on dacas memory storage by key
     * @param mapElements map containing values
     */
    public void updateMulti(HashMap<String, DacasElement> mapElements) {
        managerInMemoryCache.updateMulti(mapElements);
    }

}


