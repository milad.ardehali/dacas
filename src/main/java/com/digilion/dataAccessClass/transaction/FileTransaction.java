package com.digilion.dataAccessClass.transaction;

import com.digilion.dataAccessClass.element.DacasElement;
import com.digilion.dataAccessClass.exceptions.DacasException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.MapType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

/**
 * A thread-safe class that will append JSON transaction data to a file.
 *
 * @author milad-ardehali
 * @version 1
 * @since 17/01/2020
 */
public class FileTransaction {

    private static final JsonFactory jsonFactory = new JsonFactory();

    private final File file;
    private final Object writeLock = new Object();
    private final String dacasFileName = "dacasmemoriesvalues";
    private final String extension = ".dacas";
    private final String backupFolder = "dacas_backup";
    private final String propertiesFilePath;
    private Long defaultTTL;

    /**
     * Constructs a new instance for an output file.
     *
     * @param propertiesFilePath path of the file to append to
     * @param defaultTTL default time to live
     */
    public FileTransaction(String propertiesFilePath, Long defaultTTL) {
        this.propertiesFilePath = propertiesFilePath;
        this.defaultTTL = defaultTTL;
        this.file = Paths.get(propertiesFilePath.concat(dacasFileName).concat(extension)).toFile();
    }

    /**
     * Write values into Dacas file storage
     *
     * @param mapValueToWrite map containing values
     * @throws IOException
     */
    private boolean write(HashMap<String, DacasElement> mapValueToWrite) throws DacasException {
        // since we have multiple threads appending to the same file, synchronize to prevent concurrency issues
        synchronized (writeLock) {
            ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
            FileOutputStream outputStream;
            try {
                outputStream = new FileOutputStream(file, false);
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(outputStream, mapValueToWrite);
                return true;
            } catch (IOException e) {
                throw new DacasException(String.format("Impossible to write memory in file %s ", this.file), e.getCause());
            }
        }
    }

    /**
     * Create new Dacas file storage
     *
     * @throws DacasException dacasException
     */
    public void createNewDacasFile() throws DacasException {
        write(new HashMap<String, DacasElement>());
    }

    /**
     * Remove Dacas file storage
     */
    public void removeDacasFile() {
        if (this.file.exists())
            this.file.delete();
    }

    /**
     * Method load Dacas File if present; else it create à new empty Dacas File
     *
     * @return map containing values
     * @throws DacasException dacasException
     */
    public HashMap<String, DacasElement> read() throws DacasException {
        synchronized (writeLock) {
            HashMap<String, DacasElement> listData;
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                MapType type = objectMapper.getTypeFactory().constructMapType(HashMap.class, String.class, DacasElement.class);
                if (file.exists())
                    try {
                        listData = objectMapper.readValue(file, type);
                    } catch (JsonMappingException | JsonParseException e) {
                        throw new DacasException("Memory file not found or corrupted");
                    }
                else
                    throw new IOException();
                return listData;
            } catch (IOException e) {
                initDacasFile();
                throw new DacasException("Memory file not found. Creating new one.", e.getCause());
            }

        }
    }

    /**
     * Initialisation of dacas File
     */
    private void initDacasFile(){
        try {
            file.createNewFile();
            createNewDacasFile();
        } catch (IOException e) {
            throw new DacasException("Impossible to create memory file.", e.getCause());
        }
    }

    /**
     * Push Values into File Dacas storage
     *
     * @param mapDacasElements map containing values
     * @throws DacasException dacasException
     */
    public void push(HashMap<String, DacasElement> mapDacasElements) throws DacasException {
        CompletableFuture.completedFuture(write(mapDacasElements));
    }

    /**
     * Create backup of dacas file storage
     *
     * @return boolean
     */
    public boolean backup() {
        boolean backupCreated = false;

        Path backupFolderPath = Paths.get(this.propertiesFilePath, this.backupFolder);
        Path backupFilePath = Paths.get(getBackUpFileName(backupFolderPath));

        if (Files.notExists(backupFolderPath))
            backupFolderPath.toFile().mkdir();
        CopyOption[] options = new CopyOption[]{
                StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES
        };
        try {
            Files.copy(this.file.toPath(), backupFilePath, options);
            backupCreated = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return backupCreated;
    }

    /**
     * getBackUpFileName
     * this methode return filename
     * @param backupFolderPath back up file ename
     * @return file name
     */
    private String getBackUpFileName(Path backupFolderPath){
        LocalDateTime ldtNow = LocalDateTime.now();
        return String.valueOf(Paths.get(String.valueOf(backupFolderPath),this.dacasFileName)).concat("-backup")
                .concat(String.valueOf(ldtNow.getYear()))
                .concat(String.valueOf(ldtNow.getDayOfYear()))
                .concat(String.valueOf(ldtNow.getHour()))
                .concat(String.valueOf(ldtNow.getMinute()))
                .concat(String.valueOf(ldtNow.getSecond()))
                .concat(this.extension);

    }
}