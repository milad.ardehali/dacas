package com.digilion.dataAccessClass.manager;

import com.digilion.dataAccessClass.transaction.DacasTransaction;
import com.digilion.dataAccessClass.element.DacasElement;
import com.digilion.dataAccessClass.enumerations.DacaStorageSupportEnum;
import com.digilion.dataAccessClass.exceptions.DacasException;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <b>DacasManager</b><br>
 * Dacas manager allows you to start storage and build a Dacas element
 * @version 1
 * @since 17/01/2020
 * @author Milad ARDEHALI
 */
public class DacasManager {

    /**
     * dacas storage needed for Dacas
     *<p>
     * <u>(Thread Safe)</u>
     */
    public static DacasTransaction dacaStorage;

    /**
     * Storage support {@link DacaStorageSupportEnum}
     */
    public static DacaStorageSupportEnum storageSupport;

    /**
     * Maximum elements in memory
     */
    private int maxObject;

    /**
     * Default DacasManager constructor
     */
    private DacasManager() {
        super();
    }

    /**
     *  Constructor with two params:
     *  <ul>
     *      <li>storage support {@link DacaStorageSupportEnum}</li>
     *      <li>limit of object in memory</li>
     *      <li>file path if storage support is File; else null</li>
     *      <li>default TTL(Time To Live) in second</li>
     *  </ul>
     * @param storageSupport this is the support of storage
     * @param maxObject this is max object accepted in memory
     * @param propertiesFilePath this is the file path
     * @param defaultTTL this is the default time to live if no expired is defined
     *
     */
    public DacasManager(DacaStorageSupportEnum storageSupport, int maxObject, String propertiesFilePath, Long defaultTTL) {
    super();
        Objects.requireNonNull(defaultTTL);
        this.maxObject = maxObject;
        this.storageSupport = storageSupport;
        run(storageSupport.equals(DacaStorageSupportEnum.MEMORY) ? null : propertiesFilePath, defaultTTL);
    }

    /**
     * run dacaStorage memory only
     * @param propertiesFilePath file path
     * @param defaultTTL default time to live
     * <li>{@link DacaStorageSupportEnum}</li>
     *
     */
    private void run(String propertiesFilePath, Long defaultTTL){
        this.dacaStorage = new DacasTransaction(this.storageSupport, this.maxObject, propertiesFilePath, defaultTTL);
    }

    /**
     * load file memory if present in defined path
     */
    private void loadFileToMemory() throws DacasException{
           this.dacaStorage.read();
    }

    /**
     * This method build Dacas Element
     * @param value value to put into memory
     * @param expiredDateTime  date and time of expiration of element
     * @param <T> type of object
     * @return DacasElement in the same type
     */
    public static <T> DacasElement<T> buildDacasElement(T value, LocalDateTime expiredDateTime) {
        return new DacasElement.DacasElementBuilder<T>().value(value).lifeTime(expiredDateTime).dateTimeStorage(LocalDateTime.now()).build();
    }

}
