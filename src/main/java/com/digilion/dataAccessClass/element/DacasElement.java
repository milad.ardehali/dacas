package com.digilion.dataAccessClass.element;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;


/**
 * <h3>DacasElement</h3>
 * <span>DacasElement is recommended to use for build a container for your value to add in memory</span>
 * <span>You can also add values in generic type but use dacesElement is recommanded</span>
 * <span>Recommended to use it for insert value in memory</span>
 * @author milad-ardehali
 * @since 17/01/2020
 * @version 1
 */
public class DacasElement<T> {

    public T value;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    public LocalDateTime expiredDateTime;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    public LocalDateTime dateTimeStorage;

    /**
     * Default no argument constructor
     */
    public DacasElement() {
        super();
    }

    /**
     * All arguments constructor
     * @param value value to add un dacasElemet
     * @param expiredDateTime expire date {@link LocalDateTime}
     * @param dateTimeStorage date time of the stored element {@link LocalDateTime}
     */
    public DacasElement(T value, LocalDateTime expiredDateTime, LocalDateTime dateTimeStorage) {
        super();
        this.value = value;
        this.expiredDateTime = expiredDateTime;
        this.dateTimeStorage = dateTimeStorage;

    }

    public T getValue() {
        return value;
    }

    public LocalDateTime getExpiredDateTime() {
        return expiredDateTime;
    }

    public LocalDateTime getDateTimeStorage() {
        return dateTimeStorage;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void setExpiredDateTime(LocalDateTime expiredDateTime) {
        this.expiredDateTime = expiredDateTime;
    }

    public void setDateTimeStorage(LocalDateTime dateTimeStorage) {
        this.dateTimeStorage = dateTimeStorage;
    }

    public DacasElement(DacasElementBuilder<T> tDaCaSElementBuilder) {
        this.value = tDaCaSElementBuilder.value;
        this.expiredDateTime = tDaCaSElementBuilder.expiredDateTime;
        this.dateTimeStorage = tDaCaSElementBuilder.dateTimeStorage;
    }

    /**
     * Class DacasElementBuilder
     */
    public static class DacasElementBuilder<T>{
        private T value;
        private LocalDateTime expiredDateTime;
        protected LocalDateTime dateTimeStorage;
        public DacasElementBuilder value(T value){
            this.value=value;
            return this;
        }
        public DacasElementBuilder lifeTime(LocalDateTime expiredDateTime) {
            this.expiredDateTime = expiredDateTime;
            return this;
        }
        public DacasElementBuilder dateTimeStorage(LocalDateTime dateTimeStorage) {
            this.dateTimeStorage = dateTimeStorage;
            return this;
        }
        public DacasElement build(){
            return new DacasElement(this);
        }
    }



}
