package com.digilion.dataAccessClass.element;

import com.digilion.dataAccessClass.exceptions.DacasException;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <h3>ManagerInMemoryCache</h3>
 * <span>ManagerInMemoryCache is the brain of dacas</span>
 * <span>data values is added into ConcurrentHashMap</span>
 * <span>DacasTransaction is recommended to use for insert value in memory</span>
 * @see DacasElement
 * @author milad-ardehali
 * @since 17/01/2020
 * @version 1
 */
public class ManagerInMemoryCache<K> {

    /**
     * ConcurrentHashMap contain value in memory
     * <p>
     * <u>Thread-safe</u>
     * @see ConcurrentHashMap ConcurrentHashMap
     */
    private ConcurrentHashMap<K, DacasElement> cacheMap;

    /**
     * Default Constructor
     */
    public ManagerInMemoryCache() {
        cacheMap = new ConcurrentHashMap();

    }

    /**
     * Put value into dacas memory storage
     * @param key key of the element
     * @param value value of the element
     */
    public void put(K key, DacasElement value) {
        synchronized (cacheMap) {
            cacheMap.put(key, value);
        }
    }

    /**
     * Put multiple values into dacas memory storage
     * @param values map containing values
     */
    public void putAll(HashMap<K, DacasElement> values) {
        synchronized (cacheMap) {
            cacheMap.putAll(values);
        }
    }

    /**
     * Get value {@link DacasElement} from dacas memory storage
     * @param key key of the element to find
     * @return DacasElement
     */
    public DacasElement get(K key) {
        synchronized (cacheMap) {
            DacasElement c = cacheMap.get(key);
            if (c == null)
                return null;
            else {
                return c;
            }
        }
    }

    /**
     * Get all values from dacas memory storage
     * @return map containing values
     */
    public HashMap<String, Object> getAll() {
        synchronized (cacheMap) {
            HashMap<String, Object> dataElement = new HashMap<>();

            Iterator<Map.Entry<K, DacasElement>> iterator = cacheMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<K, DacasElement> entry = iterator.next();
                dataElement.put(entry.getKey().toString(), entry.getValue().getValue());
            }
            return dataElement;
        }
    }

    /**
     * Get all dacasElement value from dacas memory storage
     * @return map containing values
     */
    public HashMap<String, DacasElement> getAllDaCaSValues() {
        synchronized (cacheMap) {
            HashMap<String, DacasElement> dataElement = new HashMap<>();

            Iterator<Map.Entry<K, DacasElement>> iterator = cacheMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<K, DacasElement> entry = iterator.next();
                dataElement.put(entry.getKey().toString(), entry.getValue());
            }
            return dataElement;
        }
    }

    /**
     * Remove value on dacas memory storage by key
     * @param key key of the element to remove
     */
    public void remove(K key) {
        synchronized (cacheMap) {
            cacheMap.remove(key);
        }
    }

    /**
     * Update value on dacas memory storage by key
     * @param key key of the element to add
     * @param value value of the element
     * @return boolean
     */
    public boolean update(K key, DacasElement value) {
        synchronized (cacheMap) {
            if (!cacheMap.containsKey(key))
                return false;
            else {
                cacheMap.remove(key);
                cacheMap.put(key, value);
                return true;
            }
        }
    }

    /**
     * Update multiple values on dacas memory storage
     * @param values map containing values
     */
    public void updateMulti(HashMap<K, DacasElement> values) throws DacasException{
        synchronized (cacheMap) {
            values.forEach((k, v) -> {
                if(!isExist(k.toString())) {
                    throw new DacasException(String.format("Can not update element %s because it not already exist in memory. First commit it", k));
                }
                update(k,v);
            });
        }
    }

    /**
     * Get nombre of element into dacas memory storage
     * @return size
     */
    public int size() {
        synchronized (cacheMap) {
            return cacheMap.size();
        }
    }


    /**
     * Remove all element contains into dacas memory storage
     */
    public void cleanup() {
        synchronized (cacheMap) {
        cacheMap.clear();
        }
    }

    /**
     * Purge all expired elements from dacas memory storage
     * @return list of removed key
     */
    public List<String> purge() {
        synchronized (cacheMap) {
            List<String> purgedKey = new ArrayList<>();
            LocalDateTime ldtSystemNow = LocalDateTime.now();
            cacheMap.forEach((k,v) -> {
                if(v.getExpiredDateTime() != null) {
                    if (v.getExpiredDateTime().isBefore(ldtSystemNow)) {
                        remove(k);
                        purgedKey.add((String) k);
                    }
                }
            });
            return purgedKey;
        }
    }

    /**
     * Check if dacas memory storage contains this key
     * @param key key of the element to add
     * @return boolean return true if element exist
     */
    public boolean isExist(String key) {
        return cacheMap.containsKey(key) ? true : false;
    }
}